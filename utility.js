function getMaskedCharacters(query, rules){
    var output = null; var i = 0;
    while(output == null && i < rules.length){
        var mask = new RegExp(replaceMaskCharacters(rules[i++]));
        var matches = query.split(mask);
        console.log(mask, matches);
        if(matches && matches.length > 1)
            output = matches.splice(1);
    }

    return output;
}

function getMaskedCharactersObj(query, rulesArr){
    var output = null; var i = 0;
    while(output == null && i < rulesArr.length){
        var rule = rulesArr[i];
        var mask = new RegExp(replaceMaskCharacters(rule.phrase));
        var matches = query.split(mask);
        if(matches && matches.length > 1){
            output = {
                match : matches.splice(1),
                rule: rule
            };
        }
        
        i++;
    }

    return output;
}

const mask = ['qqqq', 'rrrr', 'ssss', 'tttt'];
function replaceMaskCharacters(rule){
    mask.forEach((v) =>{
        rule = rule.replace(v, "(.*)");
    });

    return rule;
}

module.exports = {
    getMaskedCharacters: getMaskedCharacters,
    getMaskedCharactersObj: getMaskedCharactersObj
}