const utility = require("../../utility");
const auth = require("../../auth");
const request = require('../../api/request').performRequest;

const groceryApiLocation = "http://localhost:4425";

function setup() {

}

const keywords = [
    { phrase: 'i have finished with my list', type: 1},
    { phrase: 'i have finished with my grocery list', type: 1},
    { phrase: 'i have finished with the list', type: 1},
    { phrase: 'i have finished with the grocery list', type: 1},
    { phrase: 'finish up my grocery list', type: 1},
    { phrase: 'finish up my list', type: 1},
    { phrase: 'finish my grocery list', type: 1},
    { phrase: 'finish my list', type: 1},
    { phrase: 'i am back from the store', type: 1},
    { phrase: 'we are back from the store', type: 1},
    { phrase: 'i am back from the grocery store', type: 1},
    { phrase: 'We are back from the grocery store', type: 1},
    { phrase: 'grocery acquired', type: 1},
    { phrase: 'got groceries', type: 1},
    { phrase: 'put the groceries into the pantry', type: 1},
    { phrase: 'put the groceries into my pantry', type: 1},
    { phrase: 'put my items into the pantry', type: 1},
    { phrase: 'move my items to the pantry', type: 1},
    { phrase: 'move my list to the pantry', type: 1},
    { phrase: 'move the list to the pantry', type: 1},
    { phrase: 'what is on my pantry list', type: 2},
    { phrase: 'what is on the pantry list', type: 2},
    { phrase: 'get me the pantry list', type: 2},
    { phrase: 'get me my pantry list', type: 2},
    { phrase: 'show me my pantry list', type: 2},
    { phrase: 'show me the pantry list', type: 2},
    { phrase: 'what is in my pantry', type: 2},
    { phrase: 'what is in the pantry', type: 2},
    { phrase: 'get me the pantry', type: 2},
    { phrase: 'get me my pantry', type: 2},
    { phrase: 'show me my pantry', type: 2},
    { phrase: 'show me the pantry', type: 2},
    { phrase: 'bring up my pantry list', type: 2},
    { phrase: 'bring up the pantry list', type: 2},
    { phrase: 'bring up my pantry', type: 2},
    { phrase: 'bring up the pantry', type: 2},
];

function GetKeywords(){
    var output = [];
    keywords.forEach(v => {
        output.push(v.phrase);
    });

    return output;
}

const intent = () => ({
    keywords: GetKeywords(),
    module: 'pantry',
});

async function pantry_resp(query){
    var item = utility.getMaskedCharactersObj(query, keywords);

    if(item != null){
        if(item.rule.type == 1){
            try{
                var res = await request(`${groceryApiLocation}/api/grocery/finish`, 'GET', null);

                return { text: 'It has been done!' };
            }
            catch(ex){
                return ex;
            }
        }
        else if(item.rule.type == 2){
            try{
                var res = await request(`${groceryApiLocation}/api/pantry`, 'GET', null);
                var sb = [];
                sb.push("Pantry List\n");
                sb.push("```\n");

                res.forEach(v => {
                    var arr = [];
                    v.unitQuantities.forEach(i => {
                        arr.push(`${i.quantity} ${i.unitName}`);
                    });
                    sb.push(`- ${arr.join(" and ")} of ${v.itemName}\n`);
                });
                sb.push("```");

                return { text: sb.join('') };
            }
            catch(ex){
                return ex;
            }
        }
    }
    
    return { text: "I could not understand your phrasing." };
}

const examples = () => [
    'what is on my pantry list',
    'can you bring up my pantry',
    'get me the pantry list',
    'show me my pantry list',
    'what is in the pantry',
    'I have finished with my grocery list',
    'finish up my list',
    'I am back from the store',
    'We are back from the grocery store',
    'grocery acquired',
    'got groceries',
    'put groceries into the pantry',
    'move my items to the pantry'
];

module.exports = {
    get: pantry_resp,
    intent,
    examples,
    setup,
};