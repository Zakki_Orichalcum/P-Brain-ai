const utility = require("../../utility");
const auth = require("../../auth");
const request = require('../../api/request').performRequest;

const groceryApiLocation = "http://localhost:4425";

function setup() {

}

const keywords = [
    { phrase: 'add the recipe qqqq to grocery', type: 1 },
    { phrase: 'add the recipe qqqq to my grocery', type: 1 },
    { phrase: 'add the recipe qqqq to the grocery', type: 1 },
    { phrase: 'add the recipe qqqq to my list', type: 1 },
    { phrase: 'add the recipe qqqq to the list', type: 1 },
    { phrase: 'add the recipe qqqq to grocery list rrrr', type: 1 },
    { phrase: 'add my recipe qqqq to grocery', type: 1 },
    { phrase: 'add my recipe qqqq to my grocery', type: 1 },
    { phrase: 'add my recipe qqqq to the grocery', type: 1 },
    { phrase: 'add my recipe qqqq to my list', type: 1 },
    { phrase: 'add my recipe qqqq to the list', type: 1 },
    { phrase: 'add my recipe qqqq to grocery list rrrr', type: 1 },
    { phrase: 'get me the recipe qqqq', type: 2},
    { phrase: 'get me my recipe qqqq', type: 2},
    { phrase: 'show me my recipe qqqq', type: 2},
    { phrase: 'show me the recipe qqqq', type: 2},
    { phrase: 'bring up my recipe qqqq', type: 2},
    { phrase: 'bring up the recipe qqqq', type: 2},
    { phrase: 'display my recipe qqqq', type: 2},
    { phrase: 'display the recipe qqqq', type: 2},
];

function GetKeywords(){
    var output = [];
    keywords.forEach(v => {
        output.push(v.phrase);
    });

    return output;
}

const intent = () => ({
    keywords: GetKeywords(),
    module: 'recipe',
});

async function recipe_resp(query){
    var item = utility.getMaskedCharactersObj(query, keywords);

    if(item != null){
        if(item.rule.type == 1){ //add recipe to grocery
            var firstItem = item.match[0];
            var subject = { recipeName: firstItem};
    
            try{
                await request(`${groceryApiLocation}/api/grocery/addrecipe`, 'GET', subject);
    
                return { text: "I added " + ((subject.amount) ? `${subject.amount} ${subject.unit} of ${subject.item}` : subject.item) + " to the grocery list." };
            }
            catch(ex){
                return ex;
            }
        }
        else if(item.rule.type == 2){ //Show recipe
            try{
                var firstItem = item.match[0];
                var subject = { recipeName: firstItem};

                var res = await request(`${groceryApiLocation}/api/recipe`, 'GET', subject);
                var sb = [];
                sb.push(res.recipeName + "\n");
                sb.push("```\n");
                sb.push('Ingredients\n')

                res.ingredients.forEach(v => {
                    sb.push(`- ${v.amount} ${v.unit.symbol} ${v.item.itemName}${(v.notes != "undefined" ? `(${v.notes})` : '')}\n`);
                });
                sb.push("\n");
                res.recipeLines.forEach((x, i)=> {
                    sb.push(`${i+1}. ${x.line}\n`);
                });
                sb.push("```");
    
                return { text: sb.join('') };
            }
            catch(ex){
                return ex;
            }
        }
    }
    
    return { text: "I could not understand the item." };
}

const examples = () => [
    'can you bring my Potato Chowder recipe',
    'add my Potato Chowder recipe to my grocery',
    'add my recipe Black Bean Burgers to my list',
    'add my recipe chicken tenders to grocery list Monday',
    'get me the recipe Margarita Pizza',
    'display the recipe Butter Beer'
];

module.exports = {
    get: recipe_resp,
    intent,
    examples,
    setup,
};