const utility = require("../../utility");
const auth = require("../../auth");
const request = require('../../api/request').performRequest;

const groceryApiLocation = "http://localhost:4425";

function setup() {

}

const keywords = [
    { phrase: 'add qqqq to grocery', type: 1 },
    { phrase: 'add qqqq to my grocery', type: 1 },
    { phrase: 'add qqqq to the grocery', type: 1 },
    { phrase: 'add qqqq to my list', type: 1 },
    { phrase: 'add qqqq to the list', type: 1 },
    { phrase: 'add qqqq to grocery list rrrr', type: 1 },
    { phrase: 'what is on my grocery list', type: 2},
    { phrase: 'what is on the grocery list', type: 2},
    { phrase: 'get me the grocery list', type: 2},
    { phrase: 'get me my grocery list', type: 2},
    { phrase: 'show me my grocery list', type: 2},
    { phrase: 'show me the grocery list', type: 2},
    { phrase: 'what is on my list', type: 2},
    { phrase: 'what is on the list', type: 2},
    { phrase: 'get me the list', type: 2},
    { phrase: 'get me my list', type: 2},
    { phrase: 'show me my list', type: 2},
    { phrase: 'show me the list', type: 2},
    { phrase: 'bring up my grocery list', type: 2},
    { phrase: 'bring up the grocery list', type: 2},
    { phrase: 'bring up my list', type: 2},
    { phrase: 'bring up the list', type: 2},
];

function GetKeywords(){
    var output = [];
    keywords.forEach(v => {
        output.push(v.phrase);
    });

    return output;
}

const intent = () => ({
    keywords: GetKeywords(),
    module: 'grocery',
});

async function grocery_resp(query){
    var item = utility.getMaskedCharactersObj(query, keywords);

    if(item != null){
        if(item.rule.type == 1){
            var firstItem = item.match[0];
            var subject = {};
            if(firstItem.indexOf('of') > -1){
                var split = firstItem.split('of');
                subject.name = split[1].trim();
                var unitSplit = split[0].split(' ');
                subject.amount = parseFloat(unitSplit[0].trim());
                subject.unit = unitSplit[1].trim();
            }
            else{
                subject.name = firstItem;
                subject.unit = "#";
                subject.amount = 1;
            }
    
            try{
                await request(`${groceryApiLocation}/api/grocery/additem`, 'POST', subject);
    
                return { text: "I added " + ((subject.amount) ? `${subject.amount} ${subject.unit} of ${subject.name}` : subject.name) + " to the grocery list." };
            }
            catch(ex){
                return ex;
            }
        }
        else if(item.rule.type == 2){
            try{
                var res = await request(`${groceryApiLocation}/api/grocery`, 'GET', null);
                var sb = [];
                sb.push("Grocery List\n");
                sb.push("```\n");

                res.forEach(v => {
                    var arr = [];
                    v.unitQuantities.forEach(i => {
                        arr.push(`${i.quantity} ${i.unitName}`);
                    });
                    sb.push(`- ${arr.join(" and ")} of ${v.itemName}\n`);
                });
                sb.push("```");
    
                return { text: sb.join('') };
            }
            catch(ex){
                return ex;
            }
        }
    }
    
    return { text: "I could not understand the item." };
}

const examples = () => [
    'can you add chicken stock to the list',
    'add cereal to my grocery',
    'add 12 oz of crackers to the list',
    'get me the grocery list',
    'what is on my list',
    'show me my list',
    'bring up the grocery list'
];

module.exports = {
    get: grocery_resp,
    intent,
    examples,
    setup,
};