const request = require('../../../api/request').queryRequest;
const auth = require('../../../auth');
const xml2js = require('xml2js')

let client = null;

async function setup() {
    // if ((await global.db.getSkillValue('fact', 'wolframalpha')) == null) {
    //     console.log('Setting default API key for Wolfram Alpha');
    //     await global.db.setSkillValue('fact', 'wolframalpha', 'U7L4VR-K3WJPLK6Y2');
    // }
    // client = wolfram.createClient(await global.db.getSkillValue('fact', 'wolframalpha'));
}

async function wlfra_resp(query) {
    var options = {
        appid: auth.wolframAlphaAppId,
        input: query
    }
    
    try {
        const result = await request('https://api.wolframalpha.com/v2/query', options);
        var data = await xml2js.parseStringPromise(result.body); 

        if(data['queryresult']['$']['success'] !== 'true'){
            console.log(data['queryresult']['$']['error']);
            return null;
        }

        var pods = data['queryresult']['pod'];

        for(var i = 0; i < pods.length; i++){
            var p = pods[i];
            if(p['$']['primary']){
                return p['subpod'][0]['plaintext'].join('');
            }
        }

    } catch (err) {
        console.log(err);
        return "I'm sorry, I didn't understand that.";
    }
}

module.exports = {
    get: wlfra_resp,
    setup,
};
