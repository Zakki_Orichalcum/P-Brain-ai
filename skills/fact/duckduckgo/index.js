const queryRequest = require('../../../api/request').queryRequest;

async function ddg_resp(query) {
    var options = {
        q : query,
        format: 'json',
        pretty: 1
    };

    let data = await queryRequest('http://api.duckduckgo.com/', options);

    try {
        data = JSON.parse(data.body);
        console.log(data);
    } catch (e) {
        if (e) {
            console.log('error parsing duckduckgo body ' + e);
        }
        return null;
    }

    const resp = data.AbstractText;

    if (resp.split('.')[1] && resp.split('.')[1] !== '') {
        return resp.split('.')[0];
    } else {
        return resp;
    }
}

module.exports = {
    get: ddg_resp,
};
