const fetch = require('node-fetch');
const querystring = require('querystring');

async function request(options) {
    if (typeof options === 'string') {
        options = {
            uri: options,
        };
    }

    const result = await fetch(options.uri || options.url, {
        ...(options.headers ? options.headers : {}),
    });
    const text = await result.text();
    if (!result.ok) {
        throw new Error(text);
    }
    return {
        body: text,
    };
}

async function queryRequest(url, params){
    var o = `${url}?${querystring.stringify(params)}`;
    console.log(o);
    return await request(o);
}

async function performRequest(endpoint, method, data) {
    return new Promise((success, error) =>{ 
        var dataString = JSON.stringify(data);

        if (method == 'GET') {
            endpoint += '?' + querystring.stringify(data);
        }
        else {
            var headers = {
                'Content-Type': 'application/json',
                'Content-Length': dataString.length
            };
        }
        var options = {
            method: method,
            headers: headers,
            body: (method != 'GET') ? dataString : null
        };

        fetch(endpoint, options)
        .then(res => {
            return res.json();
        })
        .then(d => {
            success(d);
        },
        err => {
            console.log('error', err);
            error(err.message);
        });
    });
  }

module.exports = { request, queryRequest, performRequest };
